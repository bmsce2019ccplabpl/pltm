#include <stdio.h>
int main()
{
    int i = 1,n,sum = 0;
    printf("Enter the number upto which the sum has to be found\n");
    scanf("%d",&n);
    
    while(i<=n)
    {
        sum = sum + i;
        i++;
    }
    
    printf("The sum of the first %d numbers is %d\n",n,sum);
    return 0;
}