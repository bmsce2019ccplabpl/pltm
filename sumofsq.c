#include<stdio.h>
int input();
int compute(int);
void output(int,int);

int main()
{
    int num,sum;
    num=input();
    sum=compute(num);
    output(num,sum);
    return 0;
}

int input()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}

int compute(int n)
{
  int sum=0;
  for(int i=0;i<=(2*n);i=i+2)
  {
      sum=sum+(i*i);
  }
  return sum;
}

 void output(int n,int s)
{
    printf("The sum of first %d numbers is %d\n",n,s);
}
