#include<stdio.h>
int main()
{
  struct employee
  {
    int emp_ID;
    char name[80];
    float salary;
    char DOB[80];
  };
  struct employee emp1;
  printf("Enter the employee ID\n");
  scanf("%d", &emp1.emp_ID);
  printf("Enter the employee name\n");
  scanf("%s", emp1.name);
  printf("Enter the employee salary\n");
  scanf("%f", &emp1.salary);
  printf("Enter the employee DOB\n");
  scanf("%s", &emp1.DOB);
  printf("**********EMPLOYEE DETAILS ARE AS FOLLOWS**********\n");
  printf("EMP ID : %d\n",emp1.emp_ID);
  printf("NAME : %s\n",emp1.name);
  printf("SALARY : %f\n",emp1.salary);
  printf("DOB : %s\n",emp1.DOB);
  return 0;
}